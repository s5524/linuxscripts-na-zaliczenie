#!/bin/bash

if [[ $1 = "-h" ]] #if z wywolaniem helpa
	then echo "Program to change file name to upper or lower leter"
	echo "To change leter type ./changeFileCase.sh upper lower file names"
	echo "To change all letera in all files leave file names blank"

exit 0;#wyjscie z petli
fi

if [[ $4 != "" ]] #sprawdzeie czy jest 4 argument jezeli jest to help
then echo "type -h for help"
exit 0;
fi 

if [[ $1 = "lower" ]] && [[ $2 = "upper" ]] && [[ $3 = "" ]] #petla z warunkami arumentów
	then 
for i in *;
do mv -i "$i" "$(echo "$i" | tr '[a-z]' '[A-Z]')"; done
#mv zamien nazwe w plikach -i jezeli istnieje pytaj czy zamienic. $i nazwa pliku jest przekazywana do skryptu tr ktory zmienia wszystkie litery male na duze nizej to samo ale duze na male jezeli brak 3 argumentu zmienia we wszystkich 
exit 0;

fi

if [[ $1 = "upper" ]] && [[ $2 = "lower" ]] && [[ $3 = "" ]]
        then
for i in *;
do mv -i "$i" "$(echo "$i" | tr '[A-Z]' '[a-z]')"; done

exit 0;

fi

if [[ $1 = "lower" ]] && [[ $2 = "upper" ]] && [[ $3 != "" ]]
        then
for i in $3;
do mv -i "$i" "$(echo "$i" | tr '[a-z]' '[A-Z]')"; done

exit 0;

fi

if [[ $1 = "upper" ]] && [[ $2 = "lower" ]] && [[ $3 != "" ]]
        then
for i in $3;
do mv -i "$i" "$(echo "$i" | tr '[A-Z]' '[a-z]')"; done

exit 0;

else
	echo "type -h for help"
fi

