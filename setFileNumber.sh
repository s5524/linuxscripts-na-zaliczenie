#!/bin/bash

if [[ $1 = "-h" ]];then #jezeli argument 1 to -h uruchamiamy helpa
	echo "Program will set a number in file name begining from 1";
	echo "to use script just type in bash ./setFileNumber"
exit 0;
fi
if [[ $1 = "" ]];then #jezeli brak argumentow wywołujemy skrypt
n=1;
for i in *; #dla kazdego pliku w folderze
do mv "$i" "$n"_"$i"; #zmienvnazwe z bieżącej na licze w zmiennej n i dodaj podloge
((n++));# zwieksz zmienna n o jeden
done
exit 0;
fi
if [[ $1 != "-h" ]];then #jezeli argument jest inny niz -h
        echo "Type -h for help";
exit 0;
fi

